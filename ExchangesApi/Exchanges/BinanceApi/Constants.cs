﻿namespace ExchangesApi.Exchanges.BinanceApi
{
    class Constants
    {
        public static string Endpoint => "https://api.binance.com/";
        public static string AbsolutePath => "api/v1/";
    }
}