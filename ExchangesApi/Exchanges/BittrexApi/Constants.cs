﻿namespace ExchangesApi.Exchanges.BittrexApi
{
    class Constants
    {
        public static string Endpoint => "https://bittrex.com/";
        public static string PathPublic => "api/v1.1/public/";
    }
}