﻿namespace ExchangesApi.Exchanges.LiquiApi.Data
{
    public class GenericMarket
    {
        public double High { get; set; }
        public double Low { get; set; }
        public double Avg { get; set; }
        public double Vol { get; set; }
        public double VolCur { get; set; }
        public double Last { get; set; }
        public double Buy { get; set; }
        public double Sell { get; set; }
        public int Updated { get; set; }
    }
}