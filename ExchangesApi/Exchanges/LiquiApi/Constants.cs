﻿namespace ExchangesApi.Exchanges.LiquiApi
{
    class Constants
    {
        public static string Endpoint => "https://api.liqui.io/";
        public static string AbsolutePath => "api/3/";
    }
}